#!/usr/bin/env python
# -*- coding: utf-8 -*-
import string
from translate import myTranslation
from enum import Enum


class Token():
    # token type
    eof = 'END-OF-FILE'
    ident = 'IDENT'
    strings = 'STRING'
    number = 'NUMBER'
    operator = 'OP'
    keyword = 'KEYWORD'
    st_braket = 'START_BRAKET'
    end_braket = 'END_BRAKET'
    seprator = "SEPRATOR"
    keywords = ['if', 'else', 'while', 'for', 'def', 'class', 'self', 'in', 'return', 'import', 'print', '__init__',
                'lambda', 'break']

    def __init__(self, type, value, line, line_no, line_pos):
        self.type = type
        self.value = value
        self.line = line
        self.line_pos = line_pos - len(value)
        self.line_no = line_no + 1

    def __str__(self):
        return '{0}:{1}'.format(self.line_no, self.line_pos).ljust(10) + self.type.ljust(15) + self.value


class Lexer(object):
    indentable_keywords = [' اگر', ' درغیراینصورت', ' تازمانیکه', ' برای', ' تابع', ' کلاس']
    invalid_str = "\'\"#$~?"
    other_Symbol = '\,\:\;'
    eof_sign = '$'
    whitespace = ' \t\n'
    newline = '\n'
    comment_sign = '#'
    str_sign = '\"\''
    start_braket = '[({'
    end_braket = '])}'
    state = 0

    def __init__(self, code):

        self.code = code
        self.cursor = 0
        self.tokens = []
        self.lines = code.split(Lexer.newline)
        self.line_no = 0
        self.line_pos = 0

    def get_next_char(self):
        self.cursor += 1
        self.line_pos += 1
        if self.cursor > len(self.code):
            return Lexer.eof_sign

        return self.code[self.cursor - 1]

    def this_is_Error(self, charre):
        raise ValueError('\n!!! Error Unexpected character found :Line {0}:{1} -> {2}\n{3}'.format(self.line_no + 1,
                                                                                                   self.line_pos,
                                                                                                   charre,
                                                                                                   self.lines[
                                                                                                       self.line_no]))

    def tokenise(self):
        char = self.get_next_char()
        # while char != Lexer.eof_sign:
        while self.cursor < len(self.code):
            flag = True
            if char in Lexer.whitespace:
                if char == '\t':
                    self.line_pos += 4
                if char in Lexer.newline:
                    self.line_no += 1
                    self.line_pos = 0
                char = self.get_next_char()
            #
            # ------------------------------states--------------------------#

            if self.state == 0:
                if char in (string.ascii_letters + '_'):
                    self.state = 1
                elif char in string.digits:
                    self.state = 2
                elif char == '.':
                    self.state = 22
                elif char == '\'':
                    self.state = 3
                elif char == "\"":
                    self.state = 33
                elif char in self.comment_sign:
                    self.state = 4
                elif char in '+':
                    self.state = 51
                elif char == '-':
                    self.state = 52
                elif char == '*':
                    self.state = 53
                elif char == '/':
                    self.state = 54
                elif char == '=':
                    self.state = 55
                elif char == '>':
                    self.state = 56
                elif char == '<':
                    self.state = 57
                elif char in self.other_Symbol:
                    self.state = 58
                elif char == '!':
                    self.state = 59
                elif char == '%':
                    self.state = 50
                elif char in self.start_braket:
                    self.state = 6
                elif char in self.end_braket:
                    self.state = 7
                elif char not in (self.whitespace):
                    self.state = 99
            #
            # ---------- state = 1 ----------# is ok

            if self.state == 1:
                match = char
                char = self.get_next_char()
                while char in (string.ascii_letters + string.digits + '_'):
                    match += char
                    char = self.get_next_char()

                token = Token(Token.ident, match, self.lines[self.line_no], self.line_no, self.line_pos)
                self.tokens.append(token)

                if match in Token.keywords:
                    token.type = Token.keyword

                print token

                if char in self.invalid_str:
                    if self.cursor < len(self.code):
                        self.this_is_Error(char)

                self.state = 0

            #
            # -------state = 2 ------------#
            if self.state == 2:
                match = char
                char = self.get_next_char()
                while char in string.digits:
                    match += char
                    char = self.get_next_char()
                if char != '.':
                    token = Token(Token.number, match, self.lines[self.line_no], self.line_no, self.line_pos)
                    self.tokens.append(token)
                    print token
                    self.state = 0
                else:
                    flag = False
                    self.state = 23

            if self.state == 22:
                if flag == True:
                    match = '0'
                match += char
                dot_ok = False
                float_ok = False
                char = self.get_next_char()
                if char not in (string.digits) and flag == True:  # ------------
                    dot_ok = True
                    token = Token("DOT", '.', self.lines[self.line_no], self.line_no, self.line_pos)
                    self.tokens.append(token)
                    print token
                    self.state = 0
                    # break
                else:
                    self.state = 23

            if self.state == 23:

                match += char
                char = self.get_next_char()

                while char in string.digits:
                    match += char
                    float_ok = True
                    char = self.get_next_char()
                if char not in string.digits:
                    # if flag == False:
                    #     match += '0'
                    token = Token("FLOAT", match, self.lines[self.line_no], self.line_no, self.line_pos)
                    self.tokens.append(token)
                    print token
                self.state = 0
                # flag=True

            # --------- state 3 ------------#

            if self.state == 3:
                char = self.get_next_char()
                match = ""
                while char not in ('\''):
                    match += char
                    if self.cursor > len(self.code):
                        self.this_is_Error(char)
                    char = self.get_next_char()

                self.state = 330

            if self.state == 330:
                token = Token(Token.strings, match, self.lines[self.line_no], self.line_no, self.line_pos)
                print token
                self.tokens.append(token)
                char = self.get_next_char()
                self.state = 0

            # ----------------------------------------------------------------------------state 33
            match = ""
            if self.state == 33:
                # match = ""
                char = self.get_next_char()
                if char == '\"':
                    self.state = 332
                else:
                    match += char
                    self.state = 331

            # state 331
            if self.state == 331:
                char = self.get_next_char()
                while char not in ('\"'):
                    match += char
                    if self.cursor > len(self.code):
                        self.this_is_Error(char)
                    char = self.get_next_char()
                # char = self.get_next_char()
                # token = Token(Token.strings, match, self.lines[self.line_no], self.line_no, self.line_pos)
                # self.tokens.append(token)
                # print token
                self.state = 332

            if self.state == 332:
                char = self.get_next_char()
                # if char not in ('\t' + '\n' + ' ' + '\"'):
                #     self.this_is_Error(char)
                if char == '\"':
                    self.state = 333
                else:
                    token = Token(Token.strings, match, self.lines[self.line_no], self.line_no, self.line_pos)
                    self.tokens.append(token)
                    self.state =0

                    print token

            if self.state == 333:
                char = self.get_next_char()
                while char != '\"':
                    if self.cursor < len(self.code):  # 22222222222222222222222222222222222222222222222222222
                        if char in Lexer.newline:
                            self.line_no += 1
                            self.line_pos = 0
                        char = self.get_next_char()
                    else:
                        # print '\nerror in comment'
                        self.this_is_Error(char)

                if char == '\"':
                    self.state = 334

            if self.state == 334:
                if self.cursor < len(self.code):  # 33333333333333333333333333333333333333333333
                    if char in Lexer.newline:
                        self.line_no += 1
                        self.line_pos = 0
                    char = self.get_next_char()
                    if char != '\"':
                        self.state = 333
                    elif char == '\"':
                        self.state = 335
                else:
                    self.this_is_Error(char)

            if self.state == 335:
                if self.cursor < len(self.code):  #
                    if char in Lexer.newline:
                        self.line_no += 1
                        self.line_pos = 0
                    char = self.get_next_char()

                    if char != '\"':
                        self.state = 333
                    while char == '\"':
                        char = self.get_next_char()
                    self.state = 0
                else:
                    self.this_is_Error(char)

            # end -----------------------------------------------------------------state 33
            if self.state == 55:
                match = char
                char = self.get_next_char()
                if char != '=':
                    token = Token("ASSIGMENT", match, self.lines[self.line_no], self.line_no, self.line_pos)
                    self.tokens.append(token)
                    print token
                    self.state = 0
                else:
                    match += char
                    token = Token("EQ", match, self.lines[self.line_no], self.line_no, self.line_pos)
                    self.tokens.append(token)
                    print token
                    char = self.get_next_char()
                    self.state = 0

            if self.state == 51:
                match = char
                char = self.get_next_char()
                if char != '=':
                    token = Token("PLUS", match, self.lines[self.line_no], self.line_no, self.line_pos)
                    self.tokens.append(token)
                    print token
                    self.state = 0
                else:
                    match += char
                    token = Token("SUM", match, self.lines[self.line_no], self.line_no, self.line_pos)
                    self.tokens.append(token)
                    print token
                    char = self.get_next_char()
                    self.state = 0

            if self.state == 52:
                match = char
                char = self.get_next_char()
                if char != '=':
                    token = Token("MINUS", match, self.lines[self.line_no], self.line_no, self.line_pos)
                    self.tokens.append(token)
                    print token
                    self.state = 0
                else:
                    match += char
                    token = Token("SUB", match, self.lines[self.line_no], self.line_no, self.line_pos)
                    self.tokens.append(token)
                    print token
                    char = self.get_next_char()
                    self.state = 0

            if self.state == 53:
                match = char
                char = self.get_next_char()
                if char != '=':
                    token = Token("STAR", match, self.lines[self.line_no], self.line_no, self.line_pos)
                    self.tokens.append(token)
                    print token
                    self.state = 0
                else:
                    match += char
                    token = Token("MUL", match, self.lines[self.line_no], self.line_no, self.line_pos)
                    self.tokens.append(token)
                    print token
                    char = self.get_next_char()
                    self.state = 0

            if self.state == 54:
                match = char
                char = self.get_next_char()
                if char != '=':
                    token = Token("SLASH", match, self.lines[self.line_no], self.line_no, self.line_pos)
                    self.tokens.append(token)
                    print token
                    self.state = 0
                else:
                    match += char
                    token = Token("DIVIDE", match, self.lines[self.line_no], self.line_no, self.line_pos)
                    self.tokens.append(token)

                    print token
                    char = self.get_next_char()
                    self.state = 0

            if self.state == 56:
                match = char
                char = self.get_next_char()
                if char != '=':
                    token = Token("GRT", match, self.lines[self.line_no], self.line_no, self.line_pos)
                    self.tokens.append(token)
                    print token
                    self.state = 0
                else:
                    match += char
                    token = Token("GRTEQ", match, self.lines[self.line_no], self.line_no, self.line_pos)
                    self.tokens.append(token)
                    print token
                    char = self.get_next_char()
                    self.state = 0

            if self.state == 57:
                match = char
                char = self.get_next_char()
                if char != '=':
                    token = Token("LES", match, self.lines[self.line_no], self.line_no, self.line_pos)
                    self.tokens.append(token)
                    print token
                    self.state = 0
                else:
                    match += char
                    token = Token("LESEQ", match, self.lines[self.line_no], self.line_no, self.line_pos)
                    self.tokens.append(token)
                    print token
                    char = self.get_next_char()
                    self.state = 0

            elif self.state == 58:
                token = Token("COLON", char, self.lines[self.line_no], self.line_no, self.line_pos)
                self.tokens.append(token)
                print token
                char = self.get_next_char()
                self.state = 0

            if self.state == 59:
                match = char
                char = self.get_next_char()
                if char != '=':
                    token = Token("NOT", match, self.lines[self.line_no], self.line_no, self.line_pos)
                    self.tokens.append(token)
                    print token
                    self.state = 0
                else:
                    match += char
                    token = Token("NOTEQ", match, self.lines[self.line_no], self.line_no, self.line_pos)
                    self.tokens.append(token)
                    print token
                    char = self.get_next_char()
                    self.state = 0

            if self.state == 50:
                match = char
                char = self.get_next_char()
                if char != '=':
                    token = Token("MOD", match, self.lines[self.line_no], self.line_no, self.line_pos)
                    self.tokens.append(token)
                    print token
                    self.state = 0
                else:
                    match += char
                    token = Token("MOD", match, self.lines[self.line_no], self.line_no, self.line_pos)
                    self.tokens.append(token)
                    print token
                    char = self.get_next_char()
                    self.state = 0

            if self.state == 4:
                while char not in (Lexer.newline):
                    if self.cursor < len(self.code):
                        char = self.get_next_char()

                self.state = 0

            if self.state == 6:
                token = Token(Token.st_braket , char, self.lines[self.line_no], self.line_no, self.line_pos)
                self.tokens.append(token)
                print token
                char = self.get_next_char()
                self.state = 0

            if self.state == 7:
                token = Token(Token.end_braket, char, self.lines[self.line_no], self.line_no, self.line_pos)
                self.tokens.append(token)
                print token
                char = self.get_next_char()
                self.state = 0

            # state error
            if self.state == 99:
                self.this_is_Error(char)

        # end of file token
        token = Token(Token.eof, '$', None, self.line_no, self.line_pos)
        # self.tokens.append(token)
        print token

        return self.tokens

#
# infile = open('cout.py', 'r')
# outfile = open('text2.txt', 'w+')
#
# script = infile.read()
# lexer = Lexer(script)
# infile.close()
# lexer.tokenise()
#
# infile.close()
