D [0-9]

%{

#include<stdio.h>
int lines=0,sd=0 ,row=1,col=1, words=0, sel=0, ident=0 ,num=0 ,splchr=0 ,total=0;

%}
%%
 /* this is keywords */

import {ident++ ;  printf("%d:",row);printf("%d\t",col);printf("KEYWORD \t");  ECHO ; col += yyleng;printf("\n") ;}

class {ident++ ;  printf("%d:",row);printf("%d\t",col);printf("KEYWORD \t");  ECHO ; col += yyleng;printf("\n") ;}

while {ident++ ;  printf("%d:",row);printf("%d\t",col);printf("KEYWORD \t");  ECHO ; col += yyleng;printf("\n") ;}

lambda {ident++ ;  printf("%d:",row);printf("%d\t",col);printf("KEYWORD \t");  ECHO ; col += yyleng;printf("\n") ;}

for {ident++ ;  printf("%d:",row);printf("%d\t",col);printf("KEYWORD \t");  ECHO ; col += yyleng;printf("\n") ;}

__init__ {ident++ ;  printf("%d:",row);printf("%d\t",col);printf("KEYWORD \t");  ECHO ; col += yyleng;printf("\n") ;}

def {ident++ ;  printf("%d:",row);printf("%d\t",col);printf("KEYWORD \t");  ECHO ; col += yyleng;printf("\n") ;}

from {ident++ ;  printf("%d:",row);printf("%d\t",col);printf("KEYWORD \t");  ECHO ; col += yyleng;printf("\n") ;}

print {ident++ ;  printf("%d:",row);printf("%d\t",col);printf("KEYWORD \t");  ECHO ; col += yyleng;printf("\n") ;}

if {ident++ ;  printf("%d:",row);printf("%d\t",col);printf("KEYWORD \t");  ECHO ; col += yyleng;printf("\n") ;}

else {ident++ ;  printf("%d:",row);printf("%d\t",col);printf("KEYWORD \t");  ECHO ; col += yyleng;printf("\n") ;}

elif {ident++ ;  printf("%d:",row);printf("%d\t",col);printf("KEYWORD \t");  ECHO ; col += yyleng;printf("\n") ;}

break {ident++ ;  printf("%d:",row);printf("%d\t",col);printf("KEYWORD \t");  ECHO ; col += yyleng;printf("\n") ;}

in {ident++ ;  printf("%d:",row);printf("%d\t",col);printf("KEYWORD \t");  ECHO ; col += yyleng;printf("\n") ;}

return {ident++ ;  printf("%d:",row);printf("%d\t",col);printf("KEYWORD \t");  ECHO ; col += yyleng;printf("\n") ;}

self {ident++ ;  printf("%d:",row);printf("%d\t",col);printf("KEYWORD \t");  ECHO ; col += yyleng;printf("\n") ;}

 /* this is multiline comment */
[\"]{3}[^\"]*?[\"]{3,8} {for(int i=0;i<yyleng ; i++)if(yytext[i] == '\n'){row++;col=1;}}

[\n] {row++;col=1;}

 /* I assume tabwidth = 4 */
[\t] 	col+=4;
[' ']   col++;

 /* this is identifier */
[_a-zA-z][_a-zA-z0-9]* {ident++ ;  printf("%d:",row);printf("%d\t",col);printf("IDENT \t\t");  ECHO ; col += yyleng;printf("\n") ;}

 /* this is digits and flaot */
{D}+ {  printf("%d:",row);printf("%d\t",col);printf("INT \t\t"); ECHO  ; col += yyleng;printf("\n") ;}
{D}*?[\.]{D}+ {  printf("%d:",row);printf("%d\t",col);printf("FLOAT\t\t"); ECHO  ; col += yyleng;printf("\n") ;}

 /* this is bracket */
[\[ \( \{] {  printf("%d:",row);printf("%d\t",col);printf("ST_BRACKET\t"); ECHO  ; col += yyleng;printf("\n") ;}
[\}\)\]] {  printf("%d:",row);printf("%d\t",col);printf("END_BRACKET\t"); ECHO  ; col += yyleng;printf("\n") ;}

 /* this is strings */
[\"][^\"\n]+[\"] {  printf("%d:",row);printf("%d\t",col);printf("STRING\t\t"); ECHO  ; col += yyleng;printf("\n") ;}
[\'][^\'\n]+[\'] {  printf("%d:",row);printf("%d\t",col);printf("STRING\t\t"); ECHO  ; col += yyleng;printf("\n") ;}

 /* this is operation */
[\=\+\-\*\%\/\!\>\<] {  printf("%d:",row);printf("%d\t",col);printf("OP\t\t"); ECHO  ; col += yyleng;printf("\n") ;}
[\=\+\-\*\%\/\!\>\<][\=]+? 	{ printf("%d:",row);printf("%d\t",col); if (yytext[0] == '=')printf("EQ\t\t");
								if (yytext[0] == '+')printf("SUM\t\t");
								if (yytext[0] == '-')printf("SUB\t\t");
								if (yytext[0] == '*')printf("MUL\t\t");
								if (yytext[0] == '%')printf("REMIND\t\t");
								if (yytext[0] == '/')printf("DEVIDE\t\t");
								if (yytext[0] == '>')printf("GRT\t\t");
								if (yytext[0] == '<')printf("LES\t\t");
								if (yytext[0] == '!')printf("NOT_EQ\t\t");; ECHO ; col += yyleng;printf("\n") ;}

 /* this is comment */
[#][^\n]*? ;

 /* this is others symbols */
[\,] 	 { printf("%d:",row);printf("%d\t",col);printf("CAMA\t\t"); ECHO  ; col += yyleng;printf("\n") ;}
[\;]	 {  printf("%d:",row);printf("%d\t",col);printf("SEMICOLON\t\t"); ECHO  ; col += yyleng;printf("\n") ;}
[\:]	 {  printf("%d:",row);printf("%d\t",col);printf("TWO_DOT\t\t"); ECHO  ; col += yyleng;printf("\n") ;}


. ;

%%



int main()
{
	yyin = fopen("target.py","r");
	yylex();
return 0;
}
