#!/usr/bin/env python
# -*- coding: utf-8 -*-
import string
import unicodedata
from comp_Farsi import *
from comp_python import *
# from comp_Fa_to_En import Token
# from comp_Fa_to_En import Lexer_farsi
from translate import myTranslation


class Compiling():
    # output = ""

    farsiList = []
    engList = []
    translate = myTranslation()
    list_op = ["ASSIGMENT", "EQ", "PLUS", "SUM", "MINUS", "SUB", "STAR", "MUL", "SLASH", "DIVIDE", "GRT", "GRTEQ",
               "LES", "LESEQ", "COLON", "NOT", "NOTEQ", "MOD", "DOT"]

    def persian(self, script_farsi, output):

        infile = open(script_farsi, 'r')
        outfile = open(output, 'w+')
        script_farsi = infile.read()
        lex = Lexer_farsi(script_farsi)
        fringe_line = [1]
        infile.close()
        inditi = {}
        indent_counter = 0
        indent_token = 0
        indent_flag = True
        temp = False
        try:
            for tokenn in lex.tokenise():

                if tokenn.type in (Token.ident + Token.keyword + Token.strings):
                    match_flag = True
                    if tokenn.type == Token.ident and len(self.farsiList) > 0:
                        for item in self.farsiList:
                            tempfd = tokenn.value.decode('utf-8')
                            temps = item.decode('utf-8')
                            if temps == tempfd:
                                ind = self.farsiList.index(item)
                                tokenn.value = self.engList[ind]
                                match_flag = False
                                break

                    if match_flag:
                        a = self.translate.translateToEN(tokenn.value, tokenn.type)
                        if tokenn.type == Token.ident:
                            self.farsiList.append(tokenn.value)
                            self.engList.append(a.encode('utf-8') + "")
                        tokenn.value = a.encode('utf-8') + ""

                if tokenn.type in Token.ident:
                    tokenn.value += ' '

                if tokenn.type in Token.keyword:
                    tokenn.value += ' '
                    # tr +=1
                    if indent_flag and (tokenn.value in lex.indentable_keywords):
                        temp = True
                        indent_token = tokenn.line_pos - 1
                        indent_flag = False

                if tokenn.type in self.list_op:
                    tokenn.value = ' ' + tokenn.value + ' '

                if not (tokenn.line_no in fringe_line):
                    outfile.write('\n')
                    # indent_flag = True

                    if indent_flag == True:
                        indent_counter = -1

                    if tokenn.line_pos > indent_token:
                        indent_counter += 1
                        if temp:
                            indent_counter = 0
                            temp = False
                        outfile.write(indent_counter * '\t')
                    elif tokenn.line_pos == indent_token:
                        indent_counter = inditi[tokenn.line_pos]

                        outfile.write(indent_counter * '\t')

                    else:
                        if tokenn.line_pos in inditi.keys():
                            indent_counter = inditi[tokenn.line_pos]
                            outfile.write(indent_counter * '\t')

                        else:
                            indent_counter = 0

                    if tokenn.value in lex.indentable_keywords:
                        indent_token = tokenn.line_pos
                        inditi[tokenn.line_pos] = indent_counter
                        # print inditi
                    else:
                        indent_counter -= 1
                    past_token = 0
                    # identi = 0
                    fringe_line.append(tokenn.line_no)

                outfile.write(tokenn.value)

        except ValueError as e:
            print "\n\t....!!!Value erro ! check main.py...."
            print e
        except Exception as ed:
            print ed
            print "\n\t....!!!Error....\n"
            print "1_please check your coonnection ".upper()
            print "2_please check syntax pseudo code ".upper()

        outfile.close()

    def english(self, script_python, outputs):

        infile = open(script_python, 'r')
        outfile = open(outputs, 'w+')
        script_python = infile.read()
        lex = Lexer(script_python)
        fringe_line = [1]
        infile.close()
        tr = 1
        # oval=""
        try:
            for tokenn in lex.tokenise():

                if tokenn.type in (Token.ident + Token.keyword + Token.strings + 'COLON'):
                    match_flag2 = True
                    if tokenn.type == Token.ident and len(self.engList) > 0:
                        for item in self.engList:
                            tempfd = tokenn.value.decode('utf-8')
                            temps = item.decode('utf-8')
                            if temps == tempfd:
                                ind = self.engList.index(item)
                                tokenn.value = self.farsiList[ind]
                                oval = tokenn.value
                                match_flag2 = False
                                break

                    if match_flag2:
                        a = self.translate.translateToPer(tokenn.value, tokenn.type)
                        oval = tokenn.value
                        if tokenn.type == Token.ident:
                            self.engList.append(tokenn.value)
                            self.farsiList.append(a.encode('utf-8') + "")
                        tokenn.value = a.encode('utf-8') + ""

                if tokenn.type in Token.ident:
                    tokenn.value += ' '

                if tokenn.type in Token.keyword:
                    tokenn.value = tokenn.value + ' '

                if tokenn.type in self.list_op:
                    tokenn.value = ' ' + tokenn.value + ' '

                if not (tokenn.line_no in fringe_line):
                    tr = 1
                    outfile.write('\n')
                    fringe_line.append(tokenn.line_no)

                outfile.write((tokenn.line_pos - tr) * ' ')
                outfile.write(tokenn.value)
                tr = len(oval) + tokenn.line_pos + 1
                # print oval
                # print tr
                # print len(a)

        except ValueError as e:
            print e
        except Exception as ed:
            print "\n\t....!!!Error....\n"
            print ed
            print "1_please check your coonnection ".upper()
            print "2_please check syntax pseudo code ".upper()

        outfile.close()


compiler = Compiling()

print "\nconvert to python code :"
compiler.persian('input.txt', 'cout.py')
#
# print compiler.engList
# print compiler.farsiList

print "\nconvert to persian : "
compiler.english('cout.py', 'text2.txt')
