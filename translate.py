#!/usr/bin/env python
# -*- coding: utf-8 -*-
from googletrans import Translator


class myTranslation():
    def __init__(self):
        self.eng_word = []
        self.per_word = []
        self.transing = Translator()
        self.output = None
        self.sym_lib_toper = {':': "آنگاه", 'def': 'تابع', '$': 'پایان', 'if': 'اگر',
                              'else': 'درغیراینصورت', 'from': 'از', 'in': 'در', 'range': 'دامنه', 'for': 'برای',
                              'while': 'تازمانیکه', 'self': 'خود', 'class': 'کلاس', 'break': 'خارج_شو', 'print': 'چاپ',
                              'or': 'یا', 'and': 'و', 'import': 'واردکردن'}

        self.sym_lib_toEn = {'اگر': 'if', 'تابع': 'def', 'آنگاه': ':', 'درغیراینصورت': 'else', 'رشته': 'string',
                             'از': 'from',
                             'در': 'in', 'دامنه': 'range', 'برای': 'for', 'تازمانیکه': 'while',
                             'خود': 'self', 'کلاس': 'class', 'چاپ': 'print', 'و': 'and', 'یا': 'or', 'خارج_شو': 'break',
                             'واردکردن': 'import'}

    def translateToPer(self, txt, type):
        temp = txt.split('_')
        if len(temp) > 1:
            temp1 = ""
            for item in temp:
                temp1 += self.transing.translate(item, dest='fa').text
                if not item == temp[-1]:
                    temp1 += '_'
            self.output = temp1
        else:
            if not txt in self.sym_lib_toper or type == 'STRING':

                self.output = self.transing.translate(txt, dest='fa').text

                if type != 'STRING':
                    self.output = self.output.replace(' ', '_')
                else:
                    self.output = '\'' + self.output + '\''
            else:
                self.output = self.sym_lib_toper.get(txt).decode('utf-8')
        return self.output

    def translateToEN(self, txt, type):

        if not txt in (self.sym_lib_toEn) or type == 'STRING':

            self.output = self.transing.translate(txt, dest='en').text

            if type != 'STRING':
                self.output = self.output.replace(' ', '_')
            else:
                self.output = '\'' + self.output + '\''
        else:
            self.output = self.sym_lib_toEn.get(txt).decode('utf-8')

        return self.output.lower()
